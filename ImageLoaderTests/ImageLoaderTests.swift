//
//


import XCTest
@testable import ImageLoader

class ImageLoaderTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testItemViewModel() {
        let itemRow = Row(title: "Canada", rowDescription: "Beavers are second only to humans in their ability to manipulate and change their environment", imageHref: "http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/American_Beaver.jpg/220px-American_Beaver.jpg")
        let itemViewModel = ItemViewModel(item: itemRow)
        XCTAssertEqual(itemRow.title, itemViewModel.title)
        XCTAssertEqual(itemRow.rowDescription, itemViewModel.rowDescription)
        XCTAssertEqual(itemRow.imageHref, itemViewModel.imageHref)
    }

}
