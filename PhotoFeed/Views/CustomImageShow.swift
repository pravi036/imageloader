//
//


import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

class CustomImageView: UIImageView {
    
    // MARK: Properties

    private var task: URLSessionDataTask!
    private let spinner = UIActivityIndicatorView(style: .medium)
    
    func loadImage(from url: URL) {
        image = nil
        
        // Add spinner
        addSpinner()
        
        // Cancel existing task
        if let task = task {
            task.cancel()
        }
        
        // Set image from cache
        if let imageFromCache = imageCache.object(forKey: url.absoluteString as AnyObject) as? UIImage {
            image = imageFromCache
            spinner.stopAnimating()
            return
        }
        
        // Datatask to download image from url
        task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let data = data,
                let newImage = UIImage(data: data)
            else {
                DispatchQueue.main.async {
                    self.image = UIImage(named: "placeholder")
                    self.spinner.stopAnimating()
                }
                return
            }
            //Store image into cache
            imageCache.setObject(newImage, forKey: url.absoluteString as AnyObject)
            DispatchQueue.main.async {
                self.image = newImage
                self.spinner.stopAnimating()
            }
        }
        // Start task
        task.resume()
    }
    
    // Add spinner as subview to imageview
    private func addSpinner() {
        addSubview(spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        spinner.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        spinner.startAnimating()
    }
}
