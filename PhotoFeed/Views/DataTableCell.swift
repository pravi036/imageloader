
//


import UIKit

class ItemTableViewCell: UITableViewCell {
    
    // Placeholder image
    private let placeholderImage = UIImage(named: "placeholder")
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /// Set values to UI components
    var itemViewModel:ItemViewModel? {
        didSet {
            titleLabel.text = itemViewModel?.title
            descriptionLabel.text = itemViewModel?.rowDescription
            if let urlValue = itemViewModel?.imageHref, let url = URL(string: urlValue) {
                itemImageView.loadImage(from: url)
            } else {
                itemImageView.image = placeholderImage
            }
        }
    }
    
    /// Setup Containerview
    private let containerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    /// Setup imageview
    private let itemImageView:CustomImageView = {
        var image = CustomImageView()
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    /// Setup titeLabel
    private let titleLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .white
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    /// Setup descriptionLabel
    private let descriptionLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor =  .lightGray
        label.numberOfLines = 0
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        /// Add titleLabel, imageView and descriptionLabel as subview
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(itemImageView)
        self.contentView.addSubview(descriptionLabel)
        
        /// Add constraints
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant: 10),
            titleLabel.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor, constant: -10),
            
            itemImageView.centerXAnchor.constraint(equalTo:self.contentView.centerXAnchor),
            itemImageView.topAnchor.constraint(equalTo:self.titleLabel.bottomAnchor, constant: 10),
            itemImageView.widthAnchor.constraint(equalToConstant: 150),
            itemImageView.heightAnchor.constraint(equalToConstant:100),

            descriptionLabel.topAnchor.constraint(equalTo:self.itemImageView.bottomAnchor, constant: 10),
            descriptionLabel.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant: 10),
            descriptionLabel.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor, constant: -10),
            descriptionLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor,constant: -10)
        ])
    }
}
