/*
*/

import Foundation

protocol Localizable {
    var tableName: String { get }
    var localized: String { get }
}

enum ImageLoadStrings: String, Localizable {
    case alertTitle = "There was an error fetching items."
    case confirmTitle = "Ok"
    case refreshTitle = "Fetching Data ..."
    
    var tableName: String {
        return "ImageLoader"
    }
}

extension Localizable where Self: RawRepresentable, Self.RawValue == String {
    var localized: String {
        return rawValue.localized(tableName: tableName)
    }
}

extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "\(self)", comment: "")
    }
}
