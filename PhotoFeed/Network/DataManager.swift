//
//

import Foundation

typealias FetchItemsCompletionType = (Item?, Error?) -> Void

class ItemManager {
    /// Manager class to handle success and failure response
    class func loadItems(completion: @escaping FetchItemsCompletionType) {
        ItemService.request(router: ItemRouter.getItems) { (result: Result<Item, Error>) in
            switch result {
            case .success(let responce):
                completion(responce, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
