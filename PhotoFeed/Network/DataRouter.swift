//
//


import Foundation

// Mark: - URL Components

enum ItemRouter: Router {

    case getItems
    
    var scheme: String {
        return "https"
    }

    var host: String {
        return "dl.dropboxusercontent.com"
    }

    var path: String {
        return "/s/2iodh4vg0eortkl/facts.json"
    }

    var parameters: [URLQueryItem] {
        return []
    }

    var method: String {
        return "GET"
    }
}
