

import Foundation

struct ItemViewModel {
    let title: String?
    let rowDescription: String?
    let imageHref: String?
    
    init(item: Row) {
        self.title = item.title
        self.rowDescription = item.rowDescription
        self.imageHref = item.imageHref
    }
}
