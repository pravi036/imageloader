
//


import UIKit

class ItemListViewController: UIViewController {

    // MARK: Properties
    
    private let itemsTableView = UITableView()
    private var safeArea: UILayoutGuide!
    /// Tableview identifier
    private let itemsCellReuseIdentifier = "ItemsCell"
    /// Reload tableview once item value changes
    private(set) var itemViewModels = [ItemViewModel]() {
        didSet {
            itemsTableView.reloadData()
        }
    }

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        let myAttribute = [NSAttributedString.Key.foregroundColor: UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)]
        refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
        refreshControl.attributedTitle = NSAttributedString(string: ImageLoadStrings.refreshTitle.localized, attributes: myAttribute)
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        safeArea = view.layoutMarginsGuide
        /// Setup tableview and refreshcontrol
        createView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        retrieveItems()
    }
    
    
    private func createView() {
        view.addSubview(itemsTableView)
        
        /// Register tableviewcell
        itemsTableView.register(ItemTableViewCell.self, forCellReuseIdentifier: itemsCellReuseIdentifier)
        
        /// set constraint
        itemsTableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            itemsTableView.topAnchor.constraint(equalTo:safeArea.topAnchor, constant: 10),
            itemsTableView.leftAnchor.constraint(equalTo:view.leftAnchor),
            itemsTableView.rightAnchor.constraint(equalTo:view.rightAnchor),
            itemsTableView.bottomAnchor.constraint(equalTo:view.bottomAnchor),
        ])
        
        /// Set datasource and delegate for tableview
        itemsTableView.dataSource = self
        itemsTableView.delegate = self
        
        /// Assign rowHeight as automaticDimension, so cell will expand based on content
        itemsTableView.rowHeight = UITableView.automaticDimension
        itemsTableView.estimatedRowHeight = 300
        
        /// Add refreshcontrol to tableview
        itemsTableView.refreshControl = refreshControl
    }
    
    
    private func retrieveItems() {
        ItemManager.loadItems { [weak self] (item, error) in
            guard let item = item else {
                self?.showErrorMessage(error?.localizedDescription)
                return
            }
            self?.itemViewModels = item.rows.map({return ItemViewModel(item: $0)})
            /// Set navigation item title
            self?.title = item.title
        }
    }
    
    
    @objc private func handleRefresh()
    {
        retrieveItems()
        refreshControl.endRefreshing()
    }
    
    //MARL: Helpers
    private func showErrorMessage(_ error: String?) {
        let alertController = UIAlertController(title: ImageLoadStrings.alertTitle.localized, message: error, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: ImageLoadStrings.confirmTitle.localized, style: .default)
        alertController.addAction(confirmAction)
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Datasource and Delegate

extension ItemListViewController: UITableViewDataSource, UITableViewDelegate {
    
    //MARK: UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: itemsCellReuseIdentifier, for: indexPath) as! ItemTableViewCell
        cell.backgroundColor = UIColor(white: 44.0 / 255.0, alpha: 1.0)
        cell.selectionStyle = .none
        cell.itemViewModel = itemViewModels[indexPath.row]
        return cell
    }
}

